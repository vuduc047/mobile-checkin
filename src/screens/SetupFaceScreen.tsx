import { StatusBar } from 'expo-status-bar';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, Image, Alert } from 'react-native';
import { Camera, CameraType } from 'expo-camera';
import * as FaceDetector from 'expo-face-detector';
import React, { useEffect, useRef, useState } from 'react';
import Assets from '../utils/assets';
import moment from 'moment';
import axios from 'axios';
import { manipulateAsync, FlipType, SaveFormat } from 'expo-image-manipulator';

const widthDevice = Dimensions.get('window').width;
const heighDevice = Dimensions.get('window').height;

export default function SetupFaceScreen({ route, navigation }) {
  let interval = null;
  const numberImageVerify = 15;
  let flag = null;
  const [cameraViewSize, setCameraViewSize] = useState(null);
  const cameraRef = useRef(null);
  const [faceDetectionEnabled, setEnableFace] = useState(false);
  const [faces, setFaces] = useState([]);
  const [imgBase64, setImgBase64] = useState<string[]>([]);
  let loading = false;
  const [startCamera, setStartCamera] = React.useState(false)
  const { id } = route.params;
  useEffect(() => {
    console.log('SetupFaceScreen', id)
    __startCamera();
  }, []);

  const __startCamera = async () => {
    const { status } = await Camera.requestCameraPermissionsAsync()
    console.log('status', status)
    if (status === 'granted') {
      setStartCamera(true)
    } else {
      Alert.alert('Access denied')
    }
  }

  const checkPointInsideRectange = (pointCheck) => {
    let rightCamera = {
      x: cameraViewSize.x + cameraViewSize.width,
      y: cameraViewSize.y + cameraViewSize.height - (cameraViewSize.width / 10)
    }

    let leftCamera = {
      x: cameraViewSize.x,
      y: cameraViewSize.y + (cameraViewSize.width / 10)
    }

    let conditionOne = (pointCheck.x - leftCamera.x) / (rightCamera.x - leftCamera.x) >= 0 && (pointCheck.x - leftCamera.x) / (rightCamera.x - leftCamera.x) <= 1
    let conditionTwo = (pointCheck.y - leftCamera.y) / (rightCamera.y - leftCamera.y) >= 0 && (pointCheck.y - leftCamera.y) / (rightCamera.y - leftCamera.y) <= 1

    if (conditionOne && conditionTwo) {
      return true
    } else {
      return false
    }
  }

  const checkFaceInsideCamera = (faceIdPosition) => {

    let rightFace = {
      x: faceIdPosition.origin.x + faceIdPosition.size.width,
      y: faceIdPosition.origin.y + faceIdPosition.size.height
    }

    let leftFace = {
      x: faceIdPosition.origin.x,
      y: faceIdPosition.origin.y
    }

    let topFace = {
      x: faceIdPosition.origin.x + faceIdPosition.size.width,
      y: faceIdPosition.origin.y
    }

    let bottomFace = {
      x: faceIdPosition.origin.x,
      y: faceIdPosition.origin.y + faceIdPosition.size.height
    }

    if (checkPointInsideRectange(rightFace) && checkPointInsideRectange(leftFace) && checkPointInsideRectange(topFace) && checkPointInsideRectange(bottomFace)) {
      return true
    } else {
      return false
    }

  }

  const onFacesDetected = ({ faces }) => {
    setFaces(faces)
    if (!flag && faces?.length == 1) {
      flag = new Date();
    }

    if (faces?.length === 1 && checkFaceInsideCamera(faces[0].bounds) && !loading) {
      takePicture()
    } else {
      // show warning detect facial
    }

  }
  const takePicture = async () => {
    let arrayImage = [];

    if (interval) {
      clearInterval(interval)
    }
    const options = { quality: 1, base64: true, width: widthDevice * 0.9 };
    if (imgBase64.length < numberImageVerify) {

      let data = await cameraRef.current.takePictureAsync(options);
      const reSizeImage = await manipulateAsync(data.uri,
        [{
          resize: { width: 360, height: 680 }
        }],
        {
          base64: true,
          compress: 0.5,
          format: SaveFormat.JPEG
        }
      );
      setImgBase64(imgs => [...imgs, reSizeImage.base64])
    }
    const timeVerify = Date.now() / 1000;
    console.log('SetupFaceScreen', imgBase64.length);
    if (imgBase64.length === numberImageVerify && !loading) {
      console.log('SetupFaceScreen 12', imgBase64.length);
      // set image random
      loading = true;
      setEnableFace(false)
      arrayImage = [...imgBase64];
      // call API verify
      const data = {
        "imgs": arrayImage,
        "timeVerify": timeVerify,
        "secondsTime": timeVerify
      };
      axios.post(`http://api.sylveron.com/checkin/employee/${id}/facial-recognition/setup`, data).then((res) => {
        console.log('SetupFaceScreen response detail', res);
        const response = res.data;
        Alert.alert(
          'Success',
          `Setup is done. Please wait few minutes then try to verify`,
          [
            { text: 'Yes', onPress: () => confirm() },
          ]
        )
      }).catch((e) => {
        console.log('SetupFaceScreen err detail', e);
        // Alert.alert(
        //   'Error',
        //   `Please hold the camera steady `,
        //   [
        //     { text: 'Yes', onPress: () => confirm() },
        //   ]
        // );
      })
    }


  }

  const nagivateUserList = () => {
    setStartCamera(false);
    navigation.navigate('ListUser')
  }

  const confirm = () => {
    setImgBase64(null)
    setEnableFace(true)
    loading = false;
  }

  const renderFaces = () => (
    <View style={{
      position: 'absolute',
      bottom: 0,
      right: 0,
      left: 0,
      top: 0,
    }} pointerEvents="none">
      {faces.map(renderFace)}
    </View>
  );

  const renderFace = ({ bounds, faceID, rollAngle, yawAngle }) => (
    <View
      key={faceID}
      style={[
        {
          transform: [
            { perspective: 600 },
          ]
        },
        {
          ...bounds.size,
          left: bounds.origin.x,
          top: bounds.origin.y,
        },
      ]}
    >
      <Image source={Assets.Images.imageDetextFace3} style={{ width: '100%', height: '100%', resizeMode: "contain" }} />
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.containerForm}>
        <Text style={styles.textNoteStyle}>{'Look at me'}</Text>
        <View style={styles.cameraContainer}>
          {startCamera ?
            <Camera
              onLayout={event => {
                const layout = event.nativeEvent.layout;
                setCameraViewSize(layout)
              }}
              ref={cameraRef}
              onCameraReady={() => setEnableFace(true)}
              faceDetectorSettings={{
                mode: FaceDetector.FaceDetectorMode.accurate,
                tracking: true,
                detectLandmarks: FaceDetector.FaceDetectorLandmarks.all,

              }}
              type={CameraType.front}
              style={styles.preview}
              onFacesDetected={faceDetectionEnabled ? onFacesDetected : undefined}
            >
              {!!faceDetectionEnabled && renderFaces()}
            </Camera>
            : (
              <View
                style={{
                  flex: 1,
                  backgroundColor: '#fff',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <TouchableOpacity
                  onPress={__startCamera}
                  style={{
                    width: 130,
                    borderRadius: 4,
                    backgroundColor: '#14274e',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: 40
                  }}
                >
                  <Text
                    style={{
                      color: '#fff',
                      fontWeight: 'bold',
                      textAlign: 'center'
                    }}
                  >
                    Take picture
                  </Text>
                </TouchableOpacity>
              </View>)
          }
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity style={{
            width: 130,
            borderRadius: 4,
            backgroundColor: '#14274e',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 40
          }} onPress={() => nagivateUserList()}>
            <Text style={styles.buttonText}>{'Setup faceid'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerForm: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  textNoteStyle: {
    marginVertical: 10,
    fontSize: 15,
    color: '#FFFFFF',
    height: 20,
    flex: 1,
  },
  cameraContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderWidth: 2,
    borderRadius: 5,
    overflow: 'hidden',
    flex: 15
  },
  preview: {
    width: 0.9 * widthDevice,
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionContainer: {
    flexDirection: 'row',
    minHeight: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontWeight: 'bold',
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
  },
});