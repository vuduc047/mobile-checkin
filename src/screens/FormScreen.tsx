import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity,
} from "react-native";
import { Formik } from 'formik';

export default function FormScreen() {
    const submitForm = (values) => {
        console.log('values', values);
        
    }
    return (
        <View style={styles.container}>
            <StatusBar style="auto" />
            <Formik initialValues={{ firstname: '', lastname: '' }} validateOnChange={false}
                onSubmit={values => submitForm(values)}>
                {({ handleChange, handleBlur, handleSubmit, values }) => (
                    <>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.TextInput}
                                placeholder="First-name"
                                placeholderTextColor="#003f5c"
                                onChangeText={handleChange('firstname')}
                            />
                        </View>

                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.TextInput}
                                placeholder="Last-name"
                                placeholderTextColor="#003f5c"
                                onChangeText={handleChange('lastname')}
                            />
                        </View>
                        <TouchableOpacity style={styles.loginBtn}>
                            <Button onPress={() => handleSubmit()} title="Submit" />
                        </TouchableOpacity>
                    </>
                )}
            </Formik>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    inputView: {
        backgroundColor: "#E6E6E6",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

        alignItems: "center",
    },

    TextInput: {
        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    loginBtn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        backgroundColor: "#00BFFF",
    },
});