import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button,
  Dimensions,
} from "react-native";

const heightDevice = Dimensions.get("window").height;

export default function ListUser({ navigation }) {

  const [userList, setUserList] = useState([]);

  useEffect(() => {
    getUserList();
  }, []);

  const nagivateSetupFace = (id :number) => {
    navigation.navigate("SetupFaceScreen",{id});
  };
  const getUserList = () => {
    axios.get("http://api.sylveron.com/checkin/employee/list").then((res) => {
      console.log('response detail', res);
      setUserList(res.data);
    }).catch((e) => {

    })
  }

  const clickEventListener = (item) => {
    Alert.alert(item.name);
  };
  return (
    <View style={styles.container}>
      {userList &&
        <FlatList
          style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={userList}
          horizontal={false}
          numColumns={2}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                style={styles.card}
                onPress={() => {
                  nagivateSetupFace(item.id);
                }}
              >
                <View style={styles.cardHeader}>
                  <Image
                    style={styles.icon}
                    source={{
                      uri: "https://img.icons8.com/flat_round/64/000000/hearts.png",
                    }}
                  />
                </View>
                <Image style={styles.userImage} source={{ uri: 'https://bootdey.com/img/Content/avatar/avatar6.png' }} />
                <View style={styles.cardFooter}>
                  <View
                    style={{ alignItems: "center", justifyContent: "center" }}
                  >
                    <Text style={styles.name}>{item.firstName + " " + item.lastName}</Text>
                    <Text style={styles.position}>{'Dev'}</Text>
                    <TouchableOpacity
                      style={styles.followButton}
                      onPress={() => nagivateSetupFace(item.id)}
                    >
                      <Text style={styles.followButtonText}>Add faceid</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      }
      {/* <View style={{ justifyContent: "center", flexDirection: "row" }}>
        <TouchableOpacity
          style={styles.addButton}
          onPress={() => navigation.navigate('FormScreen')}
        >
          <Text style={styles.followButtonText}>Add new user</Text>
        </TouchableOpacity>
      </View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  list: {
    paddingHorizontal: 5,
    backgroundColor: "#E6E6E6",
    // height: heightDevice * 0.7,
    flexGrow: 0,
  },
  listContainer: {
    alignItems: "center",
  },
  /******** card **************/
  card: {
    shadowColor: "#00000021",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,

    marginVertical: 5,
    backgroundColor: "white",
    flexBasis: "46%",
    marginHorizontal: 5,
  },
  cardFooter: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  userImage: {
    height: 120,
    width: 120,
    borderRadius: 60,
    alignSelf: "center",
    borderColor: "#DCDCDC",
    borderWidth: 3,
  },
  name: {
    fontSize: 18,
    flex: 1,
    alignSelf: "center",
    color: "#008080",
    fontWeight: "bold",
  },
  position: {
    fontSize: 14,
    flex: 1,
    alignSelf: "center",
    color: "#696969",
  },
  followButton: {
    marginTop: 10,
    height: 35,
    width: 100,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
    backgroundColor: "#00BFFF",
  },
  followButtonText: {
    color: "#FFFFFF",
    fontSize: 20,
  },
  icon: {
    height: 20,
    width: 20,
  },
  addButton: {
    margin: 10,
    height: 35,
    width: 300,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
    backgroundColor: "#00BFFF",
  },
});
